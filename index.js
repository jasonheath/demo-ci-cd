const express = require("express");
const app = express();
app.get("/", (req, res) => {
    res.send({hello: "world"});
});
app.get("/sum", (req, res) =>{
    var numA = parseInt(req.query.numA);
    var numB = parseInt(req.query.numB);
    var result = numA + numB;
    res.send({sum:result});
});
const PORT = process.env.PORT || 5000;
app.listen(PORT, function(){
    console.log(`App listening on port ${PORT}`);
});
